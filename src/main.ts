import * as Phaser from 'phaser'
import { GameConfig } from './game/config/game-config'

const game = new Phaser.Game(GameConfig)
