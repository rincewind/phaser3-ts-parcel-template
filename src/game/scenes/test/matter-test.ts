import * as Phaser from 'phaser'
import { GameWorld } from '../../config/game-world'
import { DefaultScene } from '../default'

export class MatterTestScene extends DefaultScene {
  constructor() {
    super('matter-test-scene')
  }

  public preload() {
    this.load.setBaseURL('http://labs.phaser.io')
    this.load.image('orb-blue', 'assets/sprites/orb-blue.png')
    this.load.image('orb-red', 'assets/sprites/orb-red.png')
    this.load.image('orb-green', 'assets/sprites/orb-green.png')
  }

  public create() {
    const ball = this.matter.add.image(GameWorld.WORLD_WIDTH / 2, 80, 'orb-red')
    this.matter.world.setBounds(
      0,
      0,
      GameWorld.WORLD_WIDTH - 16,
      GameWorld.WORLD_HEIGHT - 16,
      32,
      true,
      true,
      false,
      true
    )
    // console.log(ball.body)
    ball.body.restitution = 0.8
  }
}
