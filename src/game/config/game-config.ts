import * as Phaser from 'phaser'

import { BootScene } from '../scenes/boot'
import { MatterTestScene } from '../scenes/test/matter-test'
import { GameWorld } from './game-world'

export const GameUrl: string = 'http://127.0.0.1:4000'

export const GameConfig: Phaser.Types.Core.GameConfig = {
  type: Phaser.CANVAS,
  scene: [MatterTestScene],
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'game',
    width: GameWorld.WORLD_WIDTH,
    height: GameWorld.WORLD_HEIGHT
  },
  physics: {
    // default: 'arcade',
    // arcade: {
    //   gravity: { y: 200 }
    // }
    default: 'matter',
    matter: {
      debug: true
      // gravity: new Phaser.Math.Vector2(0, 0.9)
    }
  },
  render: {
    pixelArt: true
  }
}
